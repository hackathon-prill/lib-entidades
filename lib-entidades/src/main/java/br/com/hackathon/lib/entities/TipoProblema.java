package br.com.hackathon.lib.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.hackathon.lib.enuns.Banco;

@Entity
@Table(name = "tipo_problema", catalog = Banco.catalog)
public class TipoProblema {

	private int id;
	private String nome;
	
	public enum Fields {
		ID("id"), NOME("nome");
		
		private String description;

		private Fields(String description) {
			this.description= description;
		}

		public String toString() {
			return description;
		}
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(unique = true, nullable = false)
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoProblema other = (TipoProblema) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
