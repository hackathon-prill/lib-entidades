package br.com.hackathon.lib.entities;

import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.hackathon.lib.enuns.Banco;

@Entity
@Table(name = "problema", catalog = Banco.catalog)
public class Problema {
	private int id;
	private String email;
	private double latitude;
	private double longitude;
	private TipoProblema tipoProblema;
	private String informacoesAdicionais;
	private Criticidade criticidade;
	private String arquivoNomeDownload;
	private String arquivoChave;
	private EstadoProblema estadoProblema;
	private LocalDateTime dataInclusao;
	
	public enum Fields {
		ID("id"), 
		EMAIL("email"),
		LATITUDE("latitude"),
		LONGITUDE("longitude"),
		TIPO_PROBLEMA("tipoProblema"),
		INFORMACOES_ADICIONAIS("informacoesAdicionais"),
		CRITICIDADE("criticidade"),
		ARQUIVO_NOME_DOWNLOAD("arquivoNomeDownload"),
		ARQUIVO_CHAVE("arquivoChave"),
		ESTADO_PROBLEMA("estadoProblema"),
		DATA_INCLUSAO("dataInclusao");
		
		private String description;

		private Fields(String description) {
			this.description= description;
		}

		public String toString() {
			return description;
		}
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(nullable = false)
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	@Column(nullable = false)
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_problema_fk", nullable = false)
	public TipoProblema getTipoProblema() {
		return tipoProblema;
	}
	public void setTipoProblema(TipoProblema tipoProblema) {
		this.tipoProblema = tipoProblema;
	}
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "informacoes_adicionais", length=5)
	public String getInformacoesAdicionais() {
		return informacoesAdicionais;
	}
	public void setInformacoesAdicionais(String informacoesAdicionais) {
		this.informacoesAdicionais = informacoesAdicionais;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tipo_problema_fk", nullable = false)
	public Criticidade getCriticidade() {
		return criticidade;
	}
	public void setCriticidade(Criticidade criticidade) {
		this.criticidade = criticidade;
	}
	
	@Column(name = "arquivo_nome_download", nullable = false)
	public String getArquivoNomeDownload() {
		return arquivoNomeDownload;
	}
	public void setArquivoNomeDownload(String arquivoNomeDownload) {
		this.arquivoNomeDownload = arquivoNomeDownload;
	}
	
	@Column(name = "arquivo_chave", nullable = false)
	public String getArquivoChave() {
		return arquivoChave;
	}
	public void setArquivoChave(String arquivoChave) {
		this.arquivoChave = arquivoChave;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "estado_problema_fk", nullable = false)
	public EstadoProblema getEstadoProblema() {
		return estadoProblema;
	}
	public void setEstadoProblema(EstadoProblema estadoProblema) {
		this.estadoProblema = estadoProblema;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "estado_problema_fk", nullable = false)
	public LocalDateTime getDataInclusao() {
		return dataInclusao;
	}
	public void setDataInclusao(LocalDateTime dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Problema other = (Problema) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
